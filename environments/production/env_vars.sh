#!/bin/bash
# env variables
export ENVIRONMENT="${environment}"
export AWS_BUCKET="${aws_bucket}"
export AWS_ACCESS_KEY_ID="${aws_access_key_id}"
export AWS_SECRET_ACCESS_KEY="${aws_secret_access_key}"
export AWS_REGION="${aws_region}"